using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    GameObject hpGauge;
    GameObject PointText;
    int Point = 0;
   
    // Start is called before the first frame update
    void Start()
    {
        this.hpGauge = GameObject.Find("hpGauge");
        this.PointText = GameObject.Find("PointText");
    }

    public void DecreaseHp()
    {
        this.hpGauge.GetComponent<Image>().fillAmount -= 0.1f;

      if(this.hpGauge.GetComponent<Image>().fillAmount==0)
        {
            SceneManager.LoadScene("GAMEOVER");
        }
    }
    
    public void HealHP()
    {
        this.hpGauge.GetComponent<Image>().fillAmount += 0.1f;
    }


    public void point()
    {
        Point++;
        this.PointText.GetComponent<Text>().text = "���_" + Point.ToString("F2");


    }
}
