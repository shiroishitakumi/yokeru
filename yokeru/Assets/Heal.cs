using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "Player")
        {
            Debug.Log("a");

            GameObject director = GameObject.Find("GameDirector");
            director.GetComponent<GameDirector>().HealHP();

            Destroy(gameObject);
        }


    }

    // Update is called once per frame
    void Update()
    {

        this.transform.Translate(-0.035f, 0.0f, 0.0f);	//現在の位置から右に0.5

        if (transform.position.x < -10.0f)
        {
            Destroy(gameObject);
        }
    }
}
