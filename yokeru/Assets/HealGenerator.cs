using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealGenerator : MonoBehaviour
{
    public GameObject HealPrefab;
    float span = 0.9f;
    float delta = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(HealPrefab) as GameObject;
            float px = Random.Range(0.5f, 6);
            go.transform.position = new Vector3(7, px, 0);
        }

        

    }
}
