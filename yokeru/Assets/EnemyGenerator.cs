using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    public GameObject MummyMon;
    float span = 0.6f;
    float delta = 0;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(MummyMon) as GameObject;
            float px = Random.Range(0.5f, 9);
            go.transform.position = new Vector3(7, px, 0);
        }

        span = Random.Range(0.3f, 0.6f);
    }
}
